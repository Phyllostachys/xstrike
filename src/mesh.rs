extern crate gl;
use gl::types::*;

use std;

pub struct Mesh {
    vertex_array_object: GLuint,
    buffer_object: GLuint,
    indice_object: GLuint,
    indice_length: GLint,
}

impl Mesh {
    pub fn new() -> Self {
        Self {
            vertex_array_object: 0,
            buffer_object: 0,
            indice_object: 0,
            indice_length: 0,
        }
    }

    pub fn set_mesh(&mut self, buffer: &mut Vec<f32>, indices: &mut Vec<u32>) {
        let mut new_vao: GLuint = 0;
        let mut new_vbo: GLuint = 0;
        let mut new_ebo: GLuint = 0;
        unsafe {
            gl::GenVertexArrays(1, &mut new_vao);
            gl::BindVertexArray(new_vao);

            // vert buffer
            gl::GenBuffers(1, &mut new_vbo);
            gl::BindBuffer(gl::ARRAY_BUFFER, new_vbo);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (buffer.len() * std::mem::size_of::<f32>()) as GLsizeiptr,
                buffer.as_slice().as_ptr() as *const _,
                gl::STATIC_DRAW,
            );

            // vert attribs
            gl::EnableVertexAttribArray(0);
            gl::VertexAttribPointer(0, 3, gl::FLOAT, gl::FALSE, 0, std::ptr::null());

            // index buffer
            gl::GenBuffers(1, &mut new_ebo);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, new_ebo);
            gl::BufferData(
                gl::ELEMENT_ARRAY_BUFFER,
                (indices.len() * std::mem::size_of::<u32>()) as GLsizeiptr,
                indices.as_slice().as_ptr() as *const _,
                gl::STATIC_DRAW,
            );
        }

        self.vertex_array_object = new_vao;
        self.buffer_object = new_vbo;
        self.indice_object = new_ebo;
        self.indice_length = indices.len() as i32;
    }

    pub fn render(&self) {
        unsafe {
            gl::BindVertexArray(self.vertex_array_object);
            gl::DrawElements(
                gl::TRIANGLES,
                self.indice_length,
                gl::UNSIGNED_INT,
                std::ptr::null(),
            );
        }
    }
}
