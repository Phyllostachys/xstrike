extern crate glm;
use glm::{Matrix4, Vector3, Vector4};

use mesh::Mesh;

use texture::Texture;

pub struct GameObject {
    position: Vector3<f32>,
    scale: Vector3<f32>,
    rotation: Vector3<f32>,
    translation: Vector3<f32>,
    mesh: Mesh,
    texture: Texture,
}
