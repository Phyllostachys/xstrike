use std::ffi::CString;

extern crate gl;
use gl::types::*;

extern crate glutin;
use glutin::GlContext;

extern crate glm;
use glm::{Matrix4, Vector3, Vector4};

mod shader;
mod mesh;
mod heli;

struct Config {
    resolution: (u32, u32),
    fullscreen: bool,
}

fn main() {
    let start_time = std::time::Instant::now();

    let config = Config {
        resolution: (600, 600),
        fullscreen: false,
    };

    println!(
        "resolution = {}, {}\nfullscreen? {}",
        config.resolution.0, config.resolution.1, config.fullscreen
    );

    let mut events_loop = glutin::EventsLoop::new();
    let window = glutin::WindowBuilder::new()
        .with_title("xstrike")
        .with_dimensions(config.resolution.0, config.resolution.1);
    let context = glutin::ContextBuilder::new().with_vsync(true);
    let gl_window: glutin::GlWindow = match glutin::GlWindow::new(window, context, &events_loop) {
        Ok(w) => w,
        Err(e) => {
            println!("{}\n", e);
            std::process::exit(-1);
        }
    };

    let _ = unsafe { gl_window.make_current() };

    println!(
        "Pixel format of the window's GL context: {:?}",
        gl_window.get_pixel_format()
    );

    unsafe {
        gl::load_with(|symbol| gl_window.get_proc_address(symbol) as *const _);
        gl::ClearColor(0.3, 0.3, 0.3, 1.0);
    }

    // get shaders...
    let mut shaders: shader::Shader = shader::Shader::new(
        &String::from("shaders/basic.vert"),
        &String::from("shaders/basic.frag"),
    );
    if shaders.compile() {
        println!("Failed to compile the shaders...");
        std::process::exit(-1);
    }

    let mut heli_body = mesh::Mesh::new();
    heli_body.set_mesh(
        &mut heli::HELI_VERTICES.to_vec(),
        &mut heli::HELI_INDICES.to_vec(),
    );

    let mut heli_prop = mesh::Mesh::new();
    heli_prop.set_mesh(
        &mut heli::HELI_PROP_VERTICES.to_vec(),
        &mut heli::HELI_PROP_INDICES.to_vec(),
    );

    let _mouse_uniform_loc = unsafe {
        gl::GetUniformLocation(
            shaders.get_program(),
            CString::new("iMouse").unwrap().as_ptr(),
        )
    };

    let resolution_uniform_loc = unsafe {
        gl::GetUniformLocation(
            shaders.get_program(),
            CString::new("iResolution").unwrap().as_ptr(),
        )
    };

    let global_time_uniform_loc = unsafe {
        gl::GetUniformLocation(
            shaders.get_program(),
            CString::new("iGlobalTime").unwrap().as_ptr(),
        )
    };

    let mvp_loc = unsafe {
        gl::GetUniformLocation(shaders.get_program(), CString::new("MVP").unwrap().as_ptr())
    };

    unsafe {
        gl::Enable(gl::DEPTH_TEST);
        gl::Enable(gl::CULL_FACE);
    }

    let mat4_id: Matrix4<f32> = Matrix4::new(
        Vector4::new(1., 0., 0., 0.),
        Vector4::new(0., 1., 0., 0.),
        Vector4::new(0., 0., 1., 0.),
        Vector4::new(0., 0., 0., 1.),
    );

    let camera_pos = Vector3::new(0., 40., 100.);
    let _camera_front = Vector3::new(0., 0., -1.);
    let camera_up = Vector3::new(0., 1., 0.);

    let mut running = true;
    let (mut mouse_x, mut mouse_y): (f64, f64) = (0.0, 0.0);
    while running {
        let now = start_time.elapsed();
        let dt: f32 = now.subsec_nanos() as f32 / 1000000000.0 + now.as_secs() as f32;

        events_loop.poll_events(|event| match event {
            glutin::Event::WindowEvent { event, .. } => match event {
                glutin::WindowEvent::Closed => running = false,
                glutin::WindowEvent::Resized(w, h) => gl_window.resize(w, h),
                glutin::WindowEvent::CursorMoved {
                    position: (x, y), ..
                } => {
                    mouse_x = x;
                    mouse_y = y;
                }
                _ => (),
            },
            _ => (),
        });

        let rotation = glm::ext::rotate(&mat4_id, 0.7 * dt, Vector3::new(0., 1., 0.));
        let view = glm::ext::look_at(camera_pos, Vector3::new(0., 0., 0.), camera_up);
        let proj = glm::ext::perspective(45., 1., 0.1, 1000.);
        let mvp = proj.mul_m(&view.mul_m(&rotation));

        unsafe {
            gl::UseProgram(shaders.get_program());
            gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

            gl::UniformMatrix4fv(
                mvp_loc,
                1,
                gl::FALSE,
                std::mem::transmute::<_, *const f32>(&mvp as *const _),
            );
            gl::Uniform3f(resolution_uniform_loc, 600.0, 600.0, 0.0);
            //gl::Uniform4f(mouse_uniform_loc, mouse_x as f32, mouse_y as f32, 0.0, 0.0);
            gl::Uniform1f(global_time_uniform_loc, dt);

            //gl::PolygonMode(gl::FRONT_AND_BACK, gl::LINE);

            heli_body.render();
            heli_prop.render();
        }

        gl_window.swap_buffers().unwrap();
    }
}
