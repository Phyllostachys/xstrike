#[cfg_attr(rustfmt, rustfmt_skip)]
pub const HELI_VERTICES: [f32; 87] = [
    0.0, -3.0, 21.0,
    0.0, -1.0, 20.5,
    0.0, -5.0, 20.0,
    0.0,  0.5, 17.5,
    0.0, -1.0, 16.0,
    0.0, -5.0,  5.0,
    0.0,  3.0, 13.0,
    0.0,  2.0,  8.5,
    0.0,  0.0,  4.0,
    0.0,  4.5,  8.0,
    0.0,  5.0,  6.0,
    0.0,  6.0,  5.0,
    0.0,  6.0,  0.0,
    0.0,  6.0, -1.0,
    0.0,  7.5,  0.0,
    0.0,  7.5, -1.0,
    0.0,  0.0, -2.0,
    0.0, -4.0, -2.0,
    0.0,  6.0, -6.0,
    0.0,  5.0, -6.5,
    0.0,  0.0, -6.0,
    0.0,  5.0, -28.0,
    0.0,  2.0, -28.0,
    0.0,  0.0, -29.5,
    0.0,  1.0, -32.5,
    0.0,  3.0, -32.0,
    0.0,  4.0, -32.0,
    0.0, 10.0, -35.0,
    0.0, 10.0, -32.0,
];

#[cfg_attr(rustfmt, rustfmt_skip)]
pub const HELI_INDICES: [u32; 162] = [
     0,  2,  1,    1,  2,  0,
     1,  2,  4,    4,  2,  1,
     1,  4,  3,    3,  4,  1,
     2,  5,  4,    4,  5,  2,
     3,  4,  6,    6,  4,  3,
     4,  7,  6,    6,  7,  4,
     6,  7,  9,    9,  7,  6,
     4,  5,  7,    7,  5,  4,
     9,  7, 10,   10,  7,  9,
    10, 12, 11,   11, 12, 10,
     7,  5, 17,   17,  5,  7,
     7, 17, 20,   20, 17,  7,
     7, 20, 10,   10, 20,  7,
    14, 12, 13,   13, 12, 14,
    14, 13, 15,   15, 13, 14,
    10, 20, 12,   12, 20, 10,
    12, 20, 13,   13, 20, 12,
    13, 19, 18,   18, 19, 13,
    13, 20, 19,   19, 20, 13,
    19, 20, 22,   22, 20, 19,
    19, 22, 21,   21, 22, 19,
    22, 23, 24,   24, 23, 22,
    22, 24, 25,   25, 24, 22,
    22, 25, 21,   21, 25, 22,
    21, 25, 26,   26, 25, 21,
    21, 26, 28,   28, 26, 21,
    28, 26, 27,   27, 26, 28,
];

#[cfg_attr(rustfmt, rustfmt_skip)]
pub const HELI_PROP_VERTICES: [f32; 48] = [
     1.0,  8.0,  24.5,
    -2.0,  8.0,  24.5,
     1.0,  8.0,   0.5,
     0.0,  8.0,   0.5,
    -1.0,  8.0,   0.5,
    25.0,  8.0,   1.5,
    25.0,  8.0,  -1.5,
     1.0,  8.0,  -0.5,
    -1.0,  8.0,  -0.5,
   -25.0,  8.0,   0.5,
   -25.0,  8.0,  -2.5,
    -1.0,  8.0,  -1.5,
     0.0,  8.0,  -1.5,
     1.0,  8.0,  -1.5,
    -1.0,  8.0, -25.5,
     2.0,  8.0, -25.5,
];

#[cfg_attr(rustfmt, rustfmt_skip)]
pub const HELI_PROP_INDICES: [u32; 30] = [
     1,  0,  2,
     1,  2,  3,
     2, 13, 11,
    11,  4,  2,
     5,  6, 13,
     5, 13,  7,
    10,  9,  4,
    10,  4,  8,
    15, 14, 11,
    15, 11, 12,
];
