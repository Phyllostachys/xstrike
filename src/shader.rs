use std;
use std::str;
use std::fs::File;
use std::io::prelude::*;
use std::ffi::CString;

extern crate gl;
use gl::types::*;

pub struct Shader {
    vert_src: String,
    frag_src: String,
    prog_handle: GLuint,
    compiled: bool,
}

impl Shader {
    pub fn new(vert_file_str: &String, frag_file_str: &String) -> Shader {
        let mut vf = File::open(vert_file_str).expect("Vert file not found.\n");
        let mut vc = String::new();
        vf.read_to_string(&mut vc)
            .expect("Error reading vert file.\n");

        let mut ff = File::open(frag_file_str).expect("Frag file not found.\n");
        let mut fc = String::new();
        ff.read_to_string(&mut fc)
            .expect("Error reading frag file.\n");

        Shader {
            vert_src: vc,
            frag_src: fc,
            prog_handle: 0,
            compiled: false,
        }
    }

    fn compile_shader(&self, handle: GLuint, src: &String) {
        let shader_src = CString::new(src.as_bytes()).unwrap();
        unsafe {
            gl::ShaderSource(handle, 1, &shader_src.as_ptr(), std::ptr::null());
            gl::CompileShader(handle);

            // Get the compile status
            let mut status = gl::FALSE as GLint;
            gl::GetShaderiv(handle, gl::COMPILE_STATUS, &mut status);

            // Fail on error
            if status != (gl::TRUE as GLint) {
                let mut len = 0;
                gl::GetShaderiv(handle, gl::INFO_LOG_LENGTH, &mut len);
                let mut buf = Vec::with_capacity(len as usize);
                buf.set_len((len as usize) - 1); // subtract 1 to skip the trailing null character
                gl::GetShaderInfoLog(
                    handle,
                    len,
                    std::ptr::null_mut(),
                    buf.as_mut_ptr() as *mut GLchar,
                );
                panic!(
                    "{}",
                    str::from_utf8(&buf)
                        .ok()
                        .expect("ShaderInfoLog not valid utf8")
                );
            }
        }
    }

    pub fn compile(&mut self) -> bool {
        if self.compiled == false {
            let vert_handle: GLuint = unsafe { gl::CreateShader(gl::VERTEX_SHADER) };
            self.compile_shader(vert_handle, &self.vert_src);

            let frag_handle: GLuint = unsafe { gl::CreateShader(gl::FRAGMENT_SHADER) };
            self.compile_shader(frag_handle, &self.frag_src);

            unsafe {
                self.prog_handle = gl::CreateProgram();
                gl::AttachShader(self.prog_handle, vert_handle);
                gl::AttachShader(self.prog_handle, frag_handle);
                gl::LinkProgram(self.prog_handle);
                gl::UseProgram(self.prog_handle);

                // Get the link status
                let mut status = gl::FALSE as GLint;
                gl::GetProgramiv(self.prog_handle, gl::LINK_STATUS, &mut status);

                // Fail on error
                if status != (gl::TRUE as GLint) {
                    let mut len: GLint = 0;
                    gl::GetProgramiv(self.prog_handle, gl::INFO_LOG_LENGTH, &mut len);
                    let mut buf = Vec::with_capacity(len as usize);
                    buf.set_len((len as usize) - 1); // subtract 1 to skip the trailing null character
                    gl::GetProgramInfoLog(
                        self.prog_handle,
                        len,
                        std::ptr::null_mut(),
                        buf.as_mut_ptr() as *mut GLchar,
                    );
                    panic!(
                        "{}",
                        str::from_utf8(&buf)
                            .ok()
                            .expect("ProgramInfoLog not valid utf8")
                    );
                }
            }

            self.compiled = true;
        }

        !self.compiled
    }

    pub fn get_program(&self) -> GLuint {
        self.prog_handle
    }
}
